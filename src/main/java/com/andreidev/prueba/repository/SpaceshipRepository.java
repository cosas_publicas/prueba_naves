package com.andreidev.prueba.repository;

import com.andreidev.prueba.model.entity.SpaceshipEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SpaceshipRepository extends JpaRepository<SpaceshipEntity, Long> {

	List<SpaceshipEntity> findByNameContaining(String name);

}
