package com.andreidev.prueba.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {

	private static final Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

	@Pointcut("execution(* com.andreidev.prueba.service.impl.SpaceshipServiceImpl.findById(..))")
	public void generalPointCut() {
	}

	@Before("generalPointCut()")
	public void logBefore(JoinPoint joinpoint) {
		String methodName = joinpoint.getSignature().getName();
		Object[] args = joinpoint.getArgs();
		if (args != null) {
			for (Object arg : args) {
				if (arg instanceof Long && (Long) arg < 0) {
					logger.info("Passed negative number ({}) into method {}", arg, methodName);
				}
			}
		}
	}

}

