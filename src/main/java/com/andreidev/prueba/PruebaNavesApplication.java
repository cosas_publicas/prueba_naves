package com.andreidev.prueba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy
@EnableCaching
public class PruebaNavesApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaNavesApplication.class, args);
	}

}
