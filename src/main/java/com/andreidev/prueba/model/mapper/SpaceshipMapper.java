package com.andreidev.prueba.model.mapper;

import com.andreidev.prueba.model.dto.SpaceshipDTO;
import com.andreidev.prueba.model.entity.SpaceshipEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface SpaceshipMapper {

	SpaceshipDTO toSpaceshipDTO(SpaceshipEntity spaceshipEntity);

	SpaceshipEntity toSpaceshipEntity(SpaceshipDTO SpaceshipDto);

}
