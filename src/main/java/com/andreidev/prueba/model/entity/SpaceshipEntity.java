package com.andreidev.prueba.model.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Table(name = "SPACESHIPS")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SpaceshipEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false)
	private Long id;

	@Column(name = "NAME")
	private String name;

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "ORIGIN")
	private String origin;

	@Column(name = "ORIGIN_NAME")
	private String origin_name;

	@Column(name = "ORIGIN_DATE")
	private LocalDate origin_date;

}
