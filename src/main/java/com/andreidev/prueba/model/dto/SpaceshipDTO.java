package com.andreidev.prueba.model.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@RequiredArgsConstructor
public class SpaceshipDTO {

	private Long id;

	private String name;

	private String description;

	private String origin;

	private String origin_name;

	private LocalDate origin_date;

}
