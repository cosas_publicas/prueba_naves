package com.andreidev.prueba.service;

import com.andreidev.prueba.model.dto.SpaceshipDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface SpaceshipService {

	Page<SpaceshipDTO> findAll(Pageable pageable);

	SpaceshipDTO findById(Long id);

	List<SpaceshipDTO> findContainingName(String name);

	SpaceshipDTO createSpaceship(SpaceshipDTO spaceshipDTO);

	SpaceshipDTO updateSpaceship(SpaceshipDTO spaceshipDTO);

	Boolean deleteSpaceship(Long id);

}
