package com.andreidev.prueba.service.impl;

import com.andreidev.prueba.model.dto.SpaceshipDTO;
import com.andreidev.prueba.model.entity.SpaceshipEntity;
import com.andreidev.prueba.model.mapper.SpaceshipMapper;
import com.andreidev.prueba.repository.SpaceshipRepository;
import com.andreidev.prueba.service.SpaceshipService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SpaceshipServiceImpl implements SpaceshipService {

	private final SpaceshipRepository spaceshipRepository;

	private final SpaceshipMapper spaceshipMapper;

	@Override
	public Page<SpaceshipDTO> findAll(Pageable pageable) {
		return spaceshipRepository.findAll(pageable).map(spaceshipMapper::toSpaceshipDTO);
	}

	@Override
	@Cacheable("searchAllCache")
	public SpaceshipDTO findById(Long id) {
		SpaceshipEntity spaceship =
				spaceshipRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
						"Spaceship not found"));
		return spaceshipMapper.toSpaceshipDTO(spaceship);
	}

	@Override
	public List<SpaceshipDTO> findContainingName(String name) {
		return spaceshipRepository.findByNameContaining(name).stream().map(spaceshipMapper::toSpaceshipDTO).collect(Collectors.toList());
	}

	@Override
	public SpaceshipDTO createSpaceship(SpaceshipDTO spaceshipDTO) {
		if (spaceshipDTO.getId() != null)
			throw new ResponseStatusException(HttpStatus.CONFLICT, "A new spaceship cannot already have an ID");
		SpaceshipEntity savedEntity = spaceshipRepository.save(spaceshipMapper.toSpaceshipEntity(spaceshipDTO));
		return spaceshipMapper.toSpaceshipDTO(savedEntity);
	}

	@Override
	public SpaceshipDTO updateSpaceship(SpaceshipDTO spaceshipDTO) {
		if (spaceshipDTO.getId() == null)
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "A spaceship with this ID was not found");
		SpaceshipEntity savedEntity = spaceshipRepository.save(spaceshipMapper.toSpaceshipEntity(spaceshipDTO));
		return spaceshipMapper.toSpaceshipDTO(savedEntity);
	}

	@Override
	public Boolean deleteSpaceship(Long id) {
		spaceshipRepository.delete(spaceshipRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Spaceship not found")));
		return true;
	}
}
