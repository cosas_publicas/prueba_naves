package com.andreidev.prueba.controller;

import com.andreidev.prueba.model.dto.SpaceshipDTO;
import com.andreidev.prueba.service.SpaceshipService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/spaceships")
@RequiredArgsConstructor
public class SpaceshipController {

	private final SpaceshipService spaceshipService;

	@GetMapping
	private Page<SpaceshipDTO> findAll(Pageable pageable) {
		return spaceshipService.findAll(pageable);
	}

	@GetMapping(path = "/containsName")
	private List<SpaceshipDTO> findByName(@RequestParam String name) {
		return spaceshipService.findContainingName(name);
		// Uncomment below line to check global exception handling when calling this endpoint !
		// throw new IllegalArgumentException();
	}

	@GetMapping(path = "/findById")
	private SpaceshipDTO findById(@RequestParam Long id) {
		return spaceshipService.findById(id);
	}

	@PostMapping
	private SpaceshipDTO create(@RequestBody SpaceshipDTO spaceshipDTO) {
		return spaceshipService.createSpaceship(spaceshipDTO);
	}

	@PutMapping
	private SpaceshipDTO update(@RequestBody SpaceshipDTO spaceshipDTO) {
		return spaceshipService.updateSpaceship(spaceshipDTO);
	}

	@DeleteMapping
	private Boolean delete(@RequestParam Long id) {
		return spaceshipService.deleteSpaceship(id);
	}

}
