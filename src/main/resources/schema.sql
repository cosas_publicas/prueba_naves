CREATE TABLE IF NOT EXISTS SPACESHIPS
(
    ID          INTEGER PRIMARY KEY AUTO_INCREMENT,
    NAME        VARCHAR(255) NOT NULL,
    DESCRIPTION VARCHAR(1024),
    ORIGIN      VARCHAR(255),
    ORIGIN_NAME VARCHAR(255),
    ORIGIN_DATE DATE DEFAULT CURRENT_DATE
);
