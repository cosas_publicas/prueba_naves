package com.andreidev.prueba.unit.service;

import com.andreidev.prueba.model.dto.SpaceshipDTO;
import com.andreidev.prueba.model.entity.SpaceshipEntity;
import com.andreidev.prueba.model.mapper.SpaceshipMapper;
import com.andreidev.prueba.repository.SpaceshipRepository;
import com.andreidev.prueba.service.impl.SpaceshipServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class SpaceshipServiceImplTest {

	@InjectMocks
	SpaceshipServiceImpl spaceshipService;

	@Mock
	SpaceshipRepository spaceshipRepository;

	@Mock
	SpaceshipMapper spaceshipMapper;

	private SpaceshipEntity spaceshipEntity;
	private SpaceshipDTO spaceshipDTO;

	@BeforeEach
	void setUp() {
		spaceshipEntity = new SpaceshipEntity();
		spaceshipEntity.setId(11L);
		spaceshipEntity.setName("Test Spaceship");

		spaceshipDTO = new SpaceshipDTO();
		spaceshipDTO.setId(11L);
		spaceshipDTO.setName("Test Spaceship");
	}

	@Test
	void testFindAll() {
		Pageable pageable = PageRequest.of(0, 10);
		Page<SpaceshipEntity> spaceshipPage = new PageImpl<>(Collections.singletonList(spaceshipEntity));
		when(spaceshipRepository.findAll(pageable)).thenReturn(spaceshipPage);
		when(spaceshipMapper.toSpaceshipDTO(spaceshipEntity)).thenReturn(spaceshipDTO);

		Page<SpaceshipDTO> result = spaceshipService.findAll(pageable);

		assertNotNull(result);
		assertEquals(1, result.getTotalElements());
		assertEquals(spaceshipDTO, result.getContent().getFirst());
	}

	@Test
	void testFindById() {
		when(spaceshipRepository.findById(1L)).thenReturn(Optional.of(spaceshipEntity));
		when(spaceshipMapper.toSpaceshipDTO(spaceshipEntity)).thenReturn(spaceshipDTO);

		SpaceshipDTO result = spaceshipService.findById(1L);

		assertNotNull(result);
		assertEquals(spaceshipDTO, result);
	}

	@Test
	void testFindByIdNotFound() {
		when(spaceshipRepository.findById(1L)).thenReturn(Optional.empty());

		ResponseStatusException exception = assertThrows(ResponseStatusException.class, () -> spaceshipService.findById(1L));
		assertEquals(HttpStatus.NOT_FOUND, exception.getStatusCode());
		assertEquals("Spaceship not found", exception.getReason());
	}

	@Test
	void testFindContainingName() {
		when(spaceshipRepository.findByNameContaining("Test")).thenReturn(Collections.singletonList(spaceshipEntity));
		when(spaceshipMapper.toSpaceshipDTO(spaceshipEntity)).thenReturn(spaceshipDTO);

		List<SpaceshipDTO> result = spaceshipService.findContainingName("Test");

		assertNotNull(result);
		assertEquals(1, result.size());
		assertEquals(spaceshipDTO, result.getFirst());
	}

	@Test
	void testCreateSpaceship() {
		spaceshipDTO.setId(null);
		spaceshipEntity.setId(null);
		when(spaceshipMapper.toSpaceshipEntity(spaceshipDTO)).thenReturn(spaceshipEntity);
		when(spaceshipRepository.save(spaceshipEntity)).thenReturn(spaceshipEntity);
		when(spaceshipMapper.toSpaceshipDTO(spaceshipEntity)).thenReturn(spaceshipDTO);

		SpaceshipDTO result = spaceshipService.createSpaceship(spaceshipDTO);

		assertNotNull(result);
		assertEquals(spaceshipDTO, result);
	}

	@Test
	void testCreateSpaceshipWithId() {
		spaceshipDTO.setId(1L);

		ResponseStatusException exception = assertThrows(ResponseStatusException.class, () -> spaceshipService.createSpaceship(spaceshipDTO));
		assertEquals(HttpStatus.CONFLICT, exception.getStatusCode());
		assertEquals("A new spaceship cannot already have an ID", exception.getReason());
	}

	@Test
	void testUpdateSpaceship() {
		when(spaceshipMapper.toSpaceshipEntity(spaceshipDTO)).thenReturn(spaceshipEntity);
		when(spaceshipRepository.save(spaceshipEntity)).thenReturn(spaceshipEntity);
		when(spaceshipMapper.toSpaceshipDTO(spaceshipEntity)).thenReturn(spaceshipDTO);

		SpaceshipDTO result = spaceshipService.updateSpaceship(spaceshipDTO);

		assertNotNull(result);
		assertEquals(spaceshipDTO, result);
	}

	@Test
	void testUpdateSpaceshipWithoutId() {
		spaceshipDTO.setId(null);

		ResponseStatusException exception = assertThrows(ResponseStatusException.class, () -> spaceshipService.updateSpaceship(spaceshipDTO));
		assertEquals(HttpStatus.NOT_FOUND, exception.getStatusCode());
		assertEquals("A spaceship with this ID was not found", exception.getReason());
	}

	@Test
	void testDeleteSpaceship() {
		when(spaceshipRepository.findById(1L)).thenReturn(Optional.of(spaceshipEntity));

		Boolean result = spaceshipService.deleteSpaceship(1L);

		assertTrue(result);
		verify(spaceshipRepository, times(1)).delete(spaceshipEntity);
	}

	@Test
	void testDeleteSpaceshipNotFound() {
		when(spaceshipRepository.findById(1L)).thenReturn(Optional.empty());

		ResponseStatusException exception = assertThrows(ResponseStatusException.class, () -> spaceshipService.deleteSpaceship(1L));
		assertEquals(HttpStatus.NOT_FOUND, exception.getStatusCode());
		assertEquals("Spaceship not found", exception.getReason());
	}

}
