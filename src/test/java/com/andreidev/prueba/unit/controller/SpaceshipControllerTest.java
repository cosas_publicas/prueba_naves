package com.andreidev.prueba.unit.controller;

import com.andreidev.prueba.controller.SpaceshipController;
import com.andreidev.prueba.model.dto.SpaceshipDTO;
import com.andreidev.prueba.model.entity.SpaceshipEntity;
import com.andreidev.prueba.service.SpaceshipService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(SpaceshipController.class)
@ExtendWith(MockitoExtension.class)
public class SpaceshipControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@MockBean
	private SpaceshipService spaceshipService;

	private SpaceshipDTO spaceshipDTO;
	private SpaceshipEntity spaceshipEntity;

	@BeforeEach
	void setUp() {
		spaceshipDTO = new SpaceshipDTO();
		spaceshipDTO.setId(1L);
		spaceshipDTO.setName("Test Spaceship");

		spaceshipEntity = new SpaceshipEntity();
		spaceshipEntity.setId(1L);
		spaceshipEntity.setName("Test Spaceship");
	}

	@Test
	void testFindAll() throws Exception {
		Pageable pageable = PageRequest.of(0, 10);
		Page<SpaceshipDTO> spaceshipPage = new PageImpl<>(Collections.singletonList(spaceshipDTO));
		when(spaceshipService.findAll(pageable)).thenReturn(spaceshipPage);

		mockMvc.perform(get("/api/v1/spaceships")
						.contentType(MediaType.APPLICATION_JSON)
						.param("page", "0")
						.param("size", "10"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.content[0].id").value(spaceshipDTO.getId()))
				.andExpect(jsonPath("$.content[0].name").value(spaceshipDTO.getName()));
	}

	@Test
	void testFindByName() throws Exception {
		List<SpaceshipDTO> spaceshipList = Collections.singletonList(spaceshipDTO);
		when(spaceshipService.findContainingName("Test")).thenReturn(spaceshipList);

		mockMvc.perform(get("/api/v1/spaceships/containsName")
						.contentType(MediaType.APPLICATION_JSON)
						.param("name", "Test"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$[0].id").value(spaceshipDTO.getId()))
				.andExpect(jsonPath("$[0].name").value(spaceshipDTO.getName()));
	}

	@Test
	void testFindById() throws Exception {
		when(spaceshipService.findById(1L)).thenReturn(spaceshipDTO);

		mockMvc.perform(get("/api/v1/spaceships/findById")
						.contentType(MediaType.APPLICATION_JSON)
						.param("id", "1"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(spaceshipDTO.getId()))
				.andExpect(jsonPath("$.name").value(spaceshipDTO.getName()));
	}

	@Test
	void testCreate() throws Exception {
		when(spaceshipService.createSpaceship(any(SpaceshipDTO.class))).thenReturn(spaceshipDTO);

		mockMvc.perform(post("/api/v1/spaceships")
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(spaceshipDTO)))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(spaceshipDTO.getId()))
				.andExpect(jsonPath("$.name").value(spaceshipDTO.getName()));
	}

	@Test
	void testUpdate() throws Exception {
		when(spaceshipService.updateSpaceship(any(SpaceshipDTO.class))).thenReturn(spaceshipDTO);

		mockMvc.perform(put("/api/v1/spaceships")
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(spaceshipDTO)))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(spaceshipDTO.getId()))
				.andExpect(jsonPath("$.name").value(spaceshipDTO.getName()));
	}

	@Test
	void testDelete() throws Exception {
		when(spaceshipService.deleteSpaceship(1L)).thenReturn(true);

		mockMvc.perform(delete("/api/v1/spaceships")
						.contentType(MediaType.APPLICATION_JSON)
						.param("id", "1"))
				.andExpect(status().isOk())
				.andExpect(content().string("true"));
	}

}
