package com.andreidev.prueba;

import com.andreidev.prueba.model.entity.SpaceshipEntity;
import com.andreidev.prueba.repository.SpaceshipRepository;
import com.andreidev.prueba.service.SpaceshipService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.mockito.Mockito.*;

@SpringBootTest
@AutoConfigureMockMvc
public class SpaceshipServiceCacheTest {

	@Autowired
	private SpaceshipService spaceshipService;

	@MockBean
	private SpaceshipRepository spaceshipRepository;

	@Test
	public void testFindByIdCaching() {
		Long id = 1L;
		SpaceshipEntity spaceshipDTO = new SpaceshipEntity();
		spaceshipDTO.setId(id);
		spaceshipDTO.setName("Test Spaceship");

		when(spaceshipRepository.findById(id)).thenReturn(Optional.of(spaceshipDTO));

		// First call - should hit the database
		spaceshipService.findById(id);
		verify(spaceshipRepository, times(1)).findById(id);

		// Second call - should hit the cache
		spaceshipService.findById(id);
		verify(spaceshipRepository, times(1)).findById(id);
	}
}
